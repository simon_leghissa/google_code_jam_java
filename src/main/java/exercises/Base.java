package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Simon Leghissa on 2/13/14.
 */
public class Base {

    public static void main(String[] args) throws IOException {
        final String outFileName = args[1];
        final PrintStream out = new PrintStream(
                Files.newOutputStream(new File(outFileName).toPath())
        );
        processFile(args[0], out);
    }

    public static void processFile(String sourceFileName, PrintStream writer) throws IOException {
        final List<String> lines = Files.readAllLines(new File(sourceFileName).toPath(), Charset.defaultCharset());
        final Iterator<String> linesIterator = lines.iterator();
        final int testCases = Integer.parseInt(linesIterator.next());
        for(int i=0; i<testCases; i++){
            processTestCase(i+1, linesIterator, writer);
        }
        writer.close();
    }

    private static void processTestCase(int count, Iterator<String> linesIterator, PrintStream writer) throws FileNotFoundException {
        String line = linesIterator.next();

        writer.println(String.format("Case #%s: %s", count, getResult()));
    }

    private static String getResult() {

        return "YES";
    }

}
