package exercises.qual_2013.tictac;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Simon Leghissa on 2/13/14.
 */
public class TicTacToeTomek {

    public static void main(String[] args) throws IOException {
        final String outFileName = args[1];
        final PrintStream out = new PrintStream(
                Files.newOutputStream(new File(outFileName).toPath())
        );
        processFile(args[0], out);
    }

    public static void processFile(String sourceFileName, PrintStream writer) throws IOException {
        final List<String> lines = Files.readAllLines(new File(sourceFileName).toPath(), Charset.defaultCharset());
        final Iterator<String> linesIterator = lines.iterator();
        final int testCases = Integer.parseInt(linesIterator.next());
        for(int i=0; i<testCases; i++){
            processTestCase(i+1, linesIterator, writer);
        }
        writer.close();
    }

    private static void processTestCase(int count, Iterator<String> linesIterator, PrintStream writer) throws FileNotFoundException {
        final String map[] = new String[4];
        for(int i=0; i<4; i++){
            map[i] = linesIterator.next();
        }
        linesIterator.next();

        writer.println(String.format("Case #%s: %s", count, getResult(map)));
    }

    private static String getResult(String[] map) {
        final StringBuffer diagonal1 = new StringBuffer();
        final StringBuffer diagonal2 = new StringBuffer();
        final List<String> combinations = new ArrayList<>();
        boolean emptyCell = false;

        for(int i=0; i<map.length; i++){
            combinations.add(map[i]);
            final StringBuffer vertical = new StringBuffer();
            for(int j=0; j<map.length; j++){
                  vertical.append(map[j].charAt(i));
            }
            combinations.add(vertical.toString());
            if(map[i].contains(".")){
                emptyCell=true;
            }
            diagonal1.append(map[i].charAt(i));
            diagonal2.append(map[i].charAt(map.length - 1 - i));
        }
        combinations.add(diagonal1.toString());
        combinations.add(diagonal2.toString());

        for (String combination : combinations) {
            if(checkString('X', combination)){
                return "X won";
            }
            if(checkString('O', combination)){
                return "O won";
            }
        }
        if(emptyCell){
            return "Game has not completed";
        }
        return "Draw";
    }

    private static boolean checkString(char winner, String s) {
        int w =0;
        int t=0;
        for (char c : s.toCharArray()) {
            if(c == winner){
                w++;
            }
            if(c == 'T'){
                t++;
            }
        }
        return w == 4 || (w==3 && t==1);
    }

}
