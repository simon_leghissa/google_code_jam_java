package exercises.qual_2013.lawnmover;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Simon Leghissa on 2/13/14.
 */
public class Lawnmover {

    public static void main(String[] args) throws IOException {
        final String outFileName = args[1];
        final PrintStream out = new PrintStream(
                Files.newOutputStream(new File(outFileName).toPath())
        );
        processFile(args[0], out);
    }

    public static void processFile(String sourceFileName, PrintStream writer) throws IOException {
        final List<String> lines = Files.readAllLines(new File(sourceFileName).toPath(), Charset.defaultCharset());
        final Iterator<String> linesIterator = lines.iterator();
        final int testCases = Integer.parseInt(linesIterator.next());
        for(int i=0; i<testCases; i++){
            processTestCase(i+1, linesIterator, writer);
        }
        writer.close();
    }

    private static void processTestCase(int count, Iterator<String> linesIterator, PrintStream writer) throws FileNotFoundException {
        String line = linesIterator.next();
        String[] splitted = line.split("\\s");
        final int[][] map = new int[Integer.parseInt(splitted[0])][Integer.parseInt(splitted[1])];
        for(int i=0; i<map.length; i++){
            line = linesIterator.next();
            splitted = line.split("\\s");
            for(int j=0; j< map[0].length; j++){
                map[i][j]=Integer.parseInt(splitted[j]);
            }
        }

        writer.println(String.format("Case #%s: %s", count, getResult(map)));
    }

    private static String getResult(int[][] map) {
        final int[][] toBeCut = new int[map.length][map[0].length];
        for(int i=0; i<map.length; i++){
            Arrays.fill(toBeCut[i], 100);
        }

        for(int h=100; h>0; h--){
            boolean updated = false;
            for(int i=0; i<map.length; i++){
                if(canCutRow(i, h, map)){
                    updated = true;
                    updateRow(i, h, toBeCut);
                }
            }
            for(int i=0; i<map[0].length; i++){
                if(canCutColumn(i, h, map)){
                    updated=true;
                    updateColumn(i, h, toBeCut);
                }
            }
            if(!updated){
                break;
            }
        }
        for(int i=0; i<map.length; i++){
            if(!Arrays.equals(map[i],toBeCut[i])){
                return "NO";
            }
        }
        return "YES";
    }

    private static void updateRow(int i, int h, int[][] map) {
        for(int j=0; j<map[i].length; j++){
            map[i][j] = h;
        }
    }

    private static void updateColumn(int j, int h, int[][] map) {
        for(int i=0; i<map.length; i++){
            map[i][j] = h;
        }
    }

    private static boolean canCutRow(int i, int h, int[][] reference) {
        for(int j=0; j<reference[i].length; j++){
            if(reference[i][j] > h){
                return false;
            }
        }
        return true;
    }

    private static boolean canCutColumn(int j, int h, int[][] reference) {
        for(int i=0; i<reference.length; i++){
            if(reference[i][j] > h){
                return false;
            }
        }
        return true;
    }

}
