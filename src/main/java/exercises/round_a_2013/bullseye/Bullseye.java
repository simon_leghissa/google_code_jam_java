package exercises.round_a_2013.bullseye;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Simon Leghissa on 2/16/14.
 */
public class Bullseye {

    public static void main(String[] args) throws IOException {
        final String outFileName = args[1];
        final PrintStream out = new PrintStream(
                Files.newOutputStream(new File(outFileName).toPath())
        );
        processFile(args[0], out);
    }

    public static void processFile(String sourceFileName, PrintStream writer) throws IOException {
        final List<String> lines = Files.readAllLines(new File(sourceFileName).toPath(), Charset.defaultCharset());
        final Iterator<String> linesIterator = lines.iterator();
        final int testCases = Integer.parseInt(linesIterator.next());

        final Map<Long, Long> cirlesByPaint = new HashMap<>();
        long paint = 0;
        int c = 1;
        long max = (long)(2 * Math.pow(10, 18));
        while(paint < max){
            paint += (long)(Math.ceil(Math.pow(2*c, 2) - Math.pow(2*c-1, 2))*Math.PI);
            c++;
        }

        for(int i=0; i<testCases; i++){
            processTestCase(i+1, linesIterator, writer, cirlesByPaint);
        }
        writer.close();
    }

    private static void processTestCase(int count, Iterator<String> linesIterator, PrintStream writer, Map<Long, Long> cirlesByPaint) throws FileNotFoundException {
        String[] line = linesIterator.next().split("\\s");
        long r = Long.parseLong(line[0]);
        long t = Long.parseLong(line[1]);

        writer.println(String.format("Case #%s: %s", count, 1));
    }
}
