package exercises.qual_2014;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Simon Leghissa on 4/12/14.
 */
public class MagicTrick {


    public static void main(String[] args) throws IOException {
        final String outFileName = args[1];
        final PrintStream out = new PrintStream(
                Files.newOutputStream(new File(outFileName).toPath())
        );
        processFile(args[0], out);
    }

    public static void processFile(String sourceFileName, PrintStream writer) throws IOException {
        final List<String> lines = Files.readAllLines(new File(sourceFileName).toPath(), Charset.defaultCharset());
        final Iterator<String> linesIterator = lines.iterator();
        final int testCases = Integer.parseInt(linesIterator.next());
        for(int i=0; i<testCases; i++){
            processTestCase(i+1, linesIterator, writer);
        }
        writer.close();
    }

    private static void processTestCase(int count, Iterator<String> linesIterator, PrintStream writer) throws FileNotFoundException {
        final int firstAnswer = Integer.parseInt(linesIterator.next());
        Set<Integer>[] firstArrangement = parseArrangement(linesIterator);
        final int secondAnswer = Integer.parseInt(linesIterator.next());
        final Set<Integer>[] secondArrangement = parseArrangement(linesIterator);
        writer.println(String.format("Case #%s: %s", count, getResult(firstAnswer-1, secondAnswer-1, firstArrangement, secondArrangement)));
    }

    private static Set<Integer>[] parseArrangement(Iterator<String> linesIterator) {
        final Set<Integer>[] arrangement = new Set[4];
        for(int i=0; i<4; i++){
            final String[] splitted = linesIterator.next().split(" ");
            arrangement[i] = new HashSet<>();
            for(int j=0;j<splitted.length; j++){
                arrangement[i].add(Integer.parseInt(splitted[j]));
            }
        }
        return arrangement;
    }

    private static String getResult(int firstAnswer, int secondAnswer, Set<Integer>[] firstArrangement, Set<Integer>[] secondArrangement) {
        final Set<Integer> possibleCards = new HashSet<>();
        for (Integer card : firstArrangement[firstAnswer]) {
            if(secondArrangement[secondAnswer].contains(card)){
                possibleCards.add(card);
            }
        }
        switch(possibleCards.size()){
            case 1:
                return String.valueOf(possibleCards.iterator().next());
            case 0:
                return "Volunteer cheated!";
            default:
                return "Bad magician!";
        }
    }
}
