package exercises.qual_2014;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by Simon Leghissa on 4/12/14.
 */
public class CookieClickerAlpha {


    public static void main(String[] args) throws IOException {
        final String outFileName = args[1];
        final PrintStream out = new PrintStream(
                Files.newOutputStream(new File(outFileName).toPath())
        );
        processFile(args[0], out);
    }

    public static void processFile(String sourceFileName, PrintStream writer) throws IOException {
        final List<String> lines = Files.readAllLines(new File(sourceFileName).toPath(), Charset.defaultCharset());
        final Iterator<String> linesIterator = lines.iterator();
        final int testCases = Integer.parseInt(linesIterator.next());
        for(int i=0; i<testCases; i++){
            processTestCase(i+1, linesIterator, writer);
        }
        writer.close();
    }

    private static void processTestCase(int count, Iterator<String> linesIterator, PrintStream writer) throws FileNotFoundException {
        String[] splitted = linesIterator.next().split(" ");
        writer.println(String.format("Case #%s: %.7f", count, getResult(Double.parseDouble(splitted[0]),Double.parseDouble(splitted[1]),Double.parseDouble(splitted[2]))));
    }

    private static double getResult(double c, double f, double x) {
        final int initialProduction = 2;
        final PriorityQueue<State> queue = new PriorityQueue<>();
        queue.add(new State(x/initialProduction, initialProduction, x));
        queue.add(new State(c/initialProduction, initialProduction, c));
        while(!queue.isEmpty()){
            final State current = queue.poll();
            if(current.cookies == x){
                return current.time;
            }
            final double newProduction = current.production + f;
            queue.add(new State(current.time + x/newProduction, newProduction, x));
            queue.add(new State(current.time + c/newProduction, newProduction, c));
        }
        throw new RuntimeException("something went terribly wrong");
    }

    private static class State implements Comparable<State>{

        final double time;
        final double production;
        final double cookies;

        private State(double time, double production, double cookies) {
            this.time = time;
            this.production = production;
            this.cookies = cookies;
        }

        @Override
        public int compareTo(State o) {
            return Double.compare(time, o.time);
        }
    }
}
