package exercises.qual_2014;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Simon Leghissa on 4/12/14.
 */
public class DeceitfulWar {


    public static void main(String[] args) throws IOException {
        final String outFileName = args[1];
        final PrintStream out = new PrintStream(
                Files.newOutputStream(new File(outFileName).toPath())
        );
        processFile(args[0], out);
    }

    public static void processFile(String sourceFileName, PrintStream writer) throws IOException {
        final List<String> lines = Files.readAllLines(new File(sourceFileName).toPath(), Charset.defaultCharset());
        final Iterator<String> linesIterator = lines.iterator();
        final int testCases = Integer.parseInt(linesIterator.next());
        for(int i=0; i<testCases; i++){
            processTestCase(i+1, linesIterator, writer);
        }
        writer.close();
    }

    private static void processTestCase(int count, Iterator<String> linesIterator, PrintStream writer) throws FileNotFoundException {
        final int n = Integer.parseInt(linesIterator.next());
        final double[] naomisWeights = parseWeights(linesIterator.next().split(" "));
        final double[] kensWeights = parseWeights(linesIterator.next().split(" "));

        Arrays.sort(naomisWeights);
        Arrays.sort(kensWeights);

        writer.println(String.format("Case #%s: %s", count, getResult(n, naomisWeights, kensWeights)));
    }

    private static double[] parseWeights(String[] splitted) {
        final double[] weights = new double[splitted.length];
        for (int i=0; i<splitted.length; i++) {
            weights[i] = Double.parseDouble(splitted[i]);
        }
        return weights;
    }

    private static String getResult(int n, double[] naomisWeights, double[] kensWeights) {
        final int naomiWar = calculateNaomisScore(n, naomisWeights, kensWeights);
        final int naomiDeceitful = calculateNaomisScoreDeceitful(n, naomisWeights, kensWeights);
        return String.format("%d %d", naomiDeceitful, naomiWar);
    }

    private static int calculateNaomisScoreDeceitful(int n, double[] naomisWeights, double[] kensWeights) {
        int points = 0;
        int kensIndex =0;
        for (int i=0; i< n; i++) {
            if(naomisWeights[i] > kensWeights[kensIndex]){
                points++;
            }else{
                kensIndex++;
            }
        }
        return points;
    }

    private static int calculateNaomisScore(int n, double[] naomisWeights, double[] kensWeights) {
        int points = 0;
        int kenNext = 0;
        for (double naomi : naomisWeights) {
            boolean kenWon = false;
            for(int i=kenNext; i<n; i++){
                kenNext++;
                if(kensWeights[i] > naomi){
                    kenWon = true;
                    break;
                }
            }
            if(!kenWon){
                points++;
            }
        }
        return points;
    }
}
