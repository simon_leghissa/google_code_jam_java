package exercises.qual_2014;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Simon Leghissa on 4/12/14.
 */
public class MinesweeperMaster {


    public static void main(String[] args) throws IOException {
        final String outFileName = args[1];
        final PrintStream out = new PrintStream(
                Files.newOutputStream(new File(outFileName).toPath())
        );
        processFile(args[0], out);
    }

    public static void processFile(String sourceFileName, PrintStream writer) throws IOException {
        final List<String> lines = Files.readAllLines(new File(sourceFileName).toPath(), Charset.defaultCharset());
        final Iterator<String> linesIterator = lines.iterator();
        final int testCases = Integer.parseInt(linesIterator.next());
        for(int i=0; i<testCases; i++){
            processTestCase(i+1, linesIterator, writer);
        }
        writer.close();
    }

    private static void processTestCase(int count, Iterator<String> linesIterator, PrintStream writer) throws FileNotFoundException {
        final String[] line = linesIterator.next().split(" ");
        writer.println(String.format("Case #%s:", count));
        writer.println(String.format("%s", getResult(Integer.parseInt(line[0]), Integer.parseInt(line[1]), Integer.parseInt(line[2]))));
    }

    private static String getResult(int rows, int cols, int mines) {
        int newCols = cols;
        int newRows = rows;
        if(rows > cols){
            int rowsThatCanDelete = rows - cols;
            int rowsWithMines = mines / cols;
            int deletedRows = Math.min(rowsThatCanDelete, rowsWithMines);
            mines = mines - deletedRows*cols;
            newRows = rows - deletedRows;
        }
        while(true){
            boolean deleted = false;
            if(newRows >= newCols && mines >= newCols){
                newRows-=1;
                mines-=newCols;
                deleted = true;
            }
            if(newCols >= newRows && mines >= newRows){
                newCols-=1;
                mines-=newRows;
                deleted = true;
            }
            if(!deleted){
                break;
            }
        }


        for(int r=0; r<newRows; r++){
            //for col 0 and newCols-1
            String[][] innerMap = createPositioning(newRows, newCols, mines, r, 0);
            if(innerMap != null){
                //found do something
            }
            innerMap = createPositioning(newRows, newCols, mines, r, newCols-1);
            if(innerMap != null){
                //found do something
            }
        }


        return "YES";
    }

    private static String[][] createPositioning(int rows, int cols, int mines, int startR, int startC) {
        final String[][] map = new String[rows][cols];
        final Deque<Position> queue = new LinkedList<>();
        queue.add(new Position(startR, startC));
        map[startR][startC] = ".";
        int free = rows*cols -2; //for c and current point
        while(!queue.isEmpty()){
            final Position current = queue.poll();
            if(free == mines){
                //found, do something
            }
            List<Position> neighbours = findNeighbours(map, current);
        }
        return null;
    }

    private static int[][] neighbours =  new int[][]{
            new int[]{1,1},
            new int[]{-1,1},
            new int[]{-1,-1},
            new int[]{1,-1},
            new int[]{0,-1},
            new int[]{0,1},
            new int[]{1,0},
            new int[]{-1,0},
    };

    private static List<Position> findNeighbours(String[][] map, Position current) {
        List<Position> found = new LinkedList<>();
        for (int[] coords : neighbours) {
            int r = current.r + coords[0];
            int c = current.c + coords[1];
            if(map[r][c] == null){
                //se ogni vicino non e una mina e non ha vicini mine posso sceglierlo
                boolean hasNeighbourMines = false;
                for (int coord : coords) {
//                    if(map[][])
                }
            }
        }
        return null;
    }

    private static class Position{

        final int r,c;

        private Position(int r, int c) {
            this.r = r;
            this.c = c;
        }
    }
}
