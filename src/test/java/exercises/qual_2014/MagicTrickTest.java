package exercises.qual_2014;

import org.junit.Test;

import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by remote on 4/12/14.
 */
public class MagicTrickTest {

    @Test
    public void smallTest() throws IOException {
        String in = this.getClass().getResource("/tmp/small.txt").getFile();
        MagicTrick.processFile(in, new PrintStream("/tmp/out.txt"));
    }
}
