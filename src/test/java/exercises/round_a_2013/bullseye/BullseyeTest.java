package exercises.round_a_2013.bullseye;

import org.junit.Test;

import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by Simon Leghissa on 2/16/14.
 */
public class BullseyeTest {

    @Test
    public void smallTest() throws IOException {
        String in = this.getClass().getResource("small.txt").getFile();
        Bullseye.processFile(in, new PrintStream("target/out.txt"));
    }

    @Test
    public void largeTest() throws IOException {
        String in = this.getClass().getResource("large.txt").getFile();
        Bullseye.processFile(in, new PrintStream("target/out.txt"));
    }
}
