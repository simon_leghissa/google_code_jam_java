package exercises.qual_2013.ticatac;

import exercises.qual_2013.tictac.TicTacToeTomek;
import org.junit.Test;

import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by Simon Leghissa on 2/13/14.
 */
public class TicTacToeTomekTest {

    @Test
    public void smallTest() throws IOException {
        String in = this.getClass().getResource("small.txt").getFile();
        TicTacToeTomek.processFile(in, new PrintStream("target/out.txt"));
    }

    @Test
    public void largeTest() throws IOException {
        String in = this.getClass().getResource("large.txt").getFile();
        TicTacToeTomek.processFile(in, new PrintStream("target/out.txt"));
    }

}
