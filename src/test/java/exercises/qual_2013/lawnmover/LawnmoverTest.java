package exercises.qual_2013.lawnmover;

import org.junit.Test;

import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by Simon Leghissa on 2/14/14.
 */
public class LawnmoverTest {

    @Test
    public void smallTest() throws IOException {
        String in = this.getClass().getResource("small.txt").getFile();
        Lawnmover.processFile(in, new PrintStream("target/out.txt"));
    }

    @Test
    public void largeTest() throws IOException {
        String in = this.getClass().getResource("large.txt").getFile();
        Lawnmover.processFile(in, new PrintStream("target/out.txt"));
    }
}
