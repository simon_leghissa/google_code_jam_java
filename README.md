README

This project includes implementations of past exercises of the Google Code Jam Competition written in Java.

The input files can be downloaded from the Google Code Jam website(https://code.google.com/codejam/).